import express from "express";
import { logger, notFound } from "./middleware.js";

const server = express();
server.use(express.json());
server.use(logger);

let books = [];

server.get("/api/v1/books", (req, res) => {
  res.status(200).send(books);
});

server.get("/api/v1/books/:id", (req, res) => {
  const id = Number(req.params.id);

  const selectedBook = books.find((book) => book.id === id);

  if (selectedBook === null) return res.status(404).send();

  res.send(selectedBook);
});

server.post("/api/v1/books", (req, res) => {
  const { id, name, author, read } = req.body;

  // ASK. 1- If it is false, I get the (ID,NAME,AUTHOR AND READ NEEDED)
  // ASK. 2- How to debug in this cases?

  if (!id || !name || !author || !read)
    return res.status(400).send("ID,NAME,AUTHOR AND READ NEEDED");

  if (id && read && (typeof id !== "number" || typeof read !== "boolean")) {
    return res.status(400).send("ID must be a number, READ must be a boolean");
  }

  books = books.concat({ id, name, author, read });
  res.status(201).send("Created");
});

server.put("/api/v1/books/:id", (req, res) => {
  const { name, author, read } = req.body;
  const id = Number(req.params.id);

  const selectedBook = books.find((book) => book.id === id);

  if (selectedBook === null) return res.status(404).send();

  const updatedBook = {
    id: selectedBook.id,
    name: name ?? selectedBook.name,
    author: author ?? selectedBook.author,
    read: read ?? selectedBook.read,
  };

  books = books.map((book) => (book.id === id ? updatedBook : book));
  res.send(updatedBook);
});

server.delete("/api/v1/books/:id", (req, res) => {
  const id = Number(req.params.id);

  const newBooks = books.filter((book) => book.id !== id);

  if (newBooks.length === books.length) return res.status(404).send();

  books = newBooks;
  res.status(204).send();
});

server.use(notFound);

const PORT = 3500;
server.listen(PORT, () => console.log("Listening to port", PORT));
