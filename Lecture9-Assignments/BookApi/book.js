import express from "express";
import { validator, errorHandler, loggingMiddleware } from "./middleware.js";

const server = express();
server.use(express.json());
server.use(validator);
server.use(loggingMiddleware);
const PORT = 3500;

let books = [];

server.get("/api/v1/books", (req, res) => {
  res.status(200).send(books);
});

server.get("/api/v1/books/:id", (req, res) => {
  res.status(201).send(books.find((book) => book.id === Number(req.params.id)));
});

server.post("/api/v1/books", validator, loggingMiddleware, (req, res) => {
  const { id, name, author, read } = req.body;

  books = books.concat({ id, name, author, read });
  res.status(201).send("Created");
});

server.put("/api/v1/books/:id", validator, loggingMiddleware, (req, res) => {
  const { name, author, read } = req.body;

  const id = Number(req.params.id);
  const book = books.find((book) => book.id === id);
  if (!book) {
    return res.status(404).send();
  }

  const updatedBook = {
    id: book.id,
    name: name ?? book.name,
    author: author ?? book.author,
    read: read ?? book.read,
  };

  books = books.map((book) => (book.id === id ? updatedBook : book));
  res.status(204).send("Changed");
});

server.delete("/api/v1/books/:id", (req, res) => {
  const id = Number(req.params.id);
  const newBookList = books.filter((book) => book.id !== id);

  if (newBookList.length === books.length) {
    return res.status(404).send("Error!");
  }
  books = newBookList;
  res.status(204).send("Deleted");
});

server.use(errorHandler);

server.listen(PORT, () => console.log(`Listening ${PORT}`));
