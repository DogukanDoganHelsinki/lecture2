export const validator = (req, res, next) => {
  const { id, name, author, read } = req.body;

  if (
    !id ||
    !name ||
    !author ||
    !read ||
    typeof id !== "number" ||
    typeof read !== "boolean"
  ) {
    return res.status(404).send("You should give an id, name, author and read");
  }

  next();
};

export const loggingMiddleware = (req, res, next) => {
  console.log(`Ip adress is : ${req.ip}`);
  console.log(`App is : ${req.app}`);
  console.log(`BaseUrl is : ${req.originalUrl}`);
  console.log(`Protocol is : ${req.protocol}`);
  console.log(`SignedCookies is : ${req.signedCookies}`);
  console.log(`Subdomains is : ${req.subdomains}`);
  next();
};

export const errorHandler = (req, res) => {
  res.status(404).send("404 - Endpoint not found");
};
