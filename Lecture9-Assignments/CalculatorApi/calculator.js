import express from "express";
import { parseNumbers } from "./middleware.js";
const server = express();
const PORT = 3005;

server.use(express.json());
server.use(parseNumbers);

server.use("/add", parseNumbers, (req, res) => {
  const sum = res.locals.myNumbers.reduce((acc, cur) => acc + cur, 0);
  res.status(201).send(`${sum}`);
});

server.use("/subtract", parseNumbers, (req, res) => {
  const subtract = res.locals.myNumbers.reduce((acc, cur) => acc - cur);
  res.status(201).send(`${subtract}`);
});

server.use("/multiply", parseNumbers, (req, res) => {
  const multiply = res.locals.myNumbers.reduce((acc, cur) => acc * cur);
  res.status(201).send(`${multiply}`);
});

server.use("/divide", parseNumbers, (req, res) => {
  if (res.locals.myNumbers[0] === 0) {
    return res.send("0 divided by any number is 0");
  }
  const divide = res.locals.myNumbers.reduce((acc, cur) => acc / cur);
  res.status(201).send(`${divide}`);
});

server.listen(PORT, () => console.log(`Listening ${PORT}`));
