export const parseNumbers = (req, res, next) => {
  if (
    Object.keys(req.query)[0] !== "numbers" ||
    Object.values(req.query)[0].length <= 0
  ) {
    return res.send("0");
  }

  const myNumbers = Object.values(req.query)[0]
    .split(",")
    .map((number) => parseInt(number));

  if (myNumbers.includes(NaN)) {
    return res.status(400).send("Error- Only numbers are accepted");
  }

  res.locals.myNumbers = myNumbers;
  next();
};
