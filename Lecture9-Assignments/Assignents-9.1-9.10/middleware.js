export function logger(req, res, next) {
  console.log(new Date());
  console.log(req.method);
  console.log(req.url);
  if (Object.keys(req.body).length > 0) {
    console.log(req.body);
  }
  next();
}

export function notFound(req, res) {
  res.status(404).send("404 - Endpoint not found");
}
