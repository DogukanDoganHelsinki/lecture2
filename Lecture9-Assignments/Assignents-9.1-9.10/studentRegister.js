import express from "express";
import { logger, notFound } from "./middleware.js";
import path from "path";
import url from "url";

const server = express();
server.use(express.json());
server.use(logger);

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let students = [];

server.get("/", (req, res) => {
  console.log("GET request init!");
  res.sendFile(path.join(__dirname, "index.html"));
});

server.get("/students", (req, res) => {
  res.send(students);
});

server.get("/students/:id", (req, res) => {
  const id = Number(req.params.id);
  const student = students.find((student) => student.id === id);
  if (student === null) {
    res.status(404).send();
  }
  res.send(req.params);
});

server.post("/students", (req, res) => {
  const { id, name, email } = req.body;
  if (!id || !name || !email) {
    return res.status(400).send("Invalid student information");
  }

  students = students.concat({ id, name, email });
  res.status(201).send("Created");
});

server.put("/students/:id", (req, res) => {
  const { name, email } = req.body;
  if (!name && !email) {
    return res.status(400).send("Invalid student information");
  }

  const id = Number(req.params.id);
  const student = students.find((student) => student.id === id);
  if (!student) {
    return res.status(404).send();
  }

  const updatedStudent = {
    id: student.id,
    name: name ?? student.name,
    email: email ?? student.email,
  };

  students = students.map((student) =>
    student.id === id ? updatedStudent : student
  );
  res.status(204).send("Created");
});

server.delete("/student/:id", (req, res) => {
  const id = Number(req.params.id);

  const newStudents = students.find((student) => student.id !== id);
  if (newStudents.length === students.length) {
    return res.status(404).send();
  }
  students = newStudents;
  res.status(204).send();
});

server.use(notFound);
server.listen(3500);
