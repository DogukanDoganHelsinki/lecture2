// Assignment 4.8: Above Average

const average = (array) => {
  const sum = array.reduce((acc, cur) => acc + cur, 0) / array.length;
  return array.filter((number) => number > sum);
};

console.log(average([1, 5, 9, 3, 4, 2, 1]));
