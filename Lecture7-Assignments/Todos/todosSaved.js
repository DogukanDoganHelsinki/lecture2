// Assignment 7.6: Todos - 1

const fetchDataUsers = async () => {
    // eslint-disable-next-line no-undef
    const todosResponse = await axios.get(
      "https://jsonplaceholder.typicode.com/todos/"
    );
  
    const todosData = todosResponse.data;
  
    for (let i = 0; i < todosData.length; i++) {
      const item = todosData[i]
      const newUser = await axios
        .get(`https://jsonplaceholder.typicode.com/users/${item.userId}`)
        .then(user => {
          
        })
        todosData[i].user = newUser.data
  
    const newTData = todosData.map((item) => {
      // eslint-disable-next-line no-undef
      return axios
        .get(`https://jsonplaceholder.typicode.com/users/${item.userId}`)
        .then((responseUser) => {
          const newUser = {
            id: item.id,
            title: item.title,
            completed: item.completed,
            user: {
              name: responseUser.data.name,
              username: responseUser.data.username,
              email: responseUser.data.email,
            },
          };
          return newUser;
        });
    });
  
    return newTData;
  };
  
  const eventListeners = () => {
    document.querySelector("#fetchData_btn1").addEventListener("click", () => {
      console.log(fetchDataUsers());
    });
  };
  
  eventListeners();
  