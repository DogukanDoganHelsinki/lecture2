// Assignment 7.6: Todos - 1

const fetchDataUsers = async () => {
  // eslint-disable-next-line no-undef
  const todosResponse = await axios.get(
    "https://jsonplaceholder.typicode.com/todos/"
  );

  const todosData = todosResponse.data;

  const newTData = todosData.map((item) => {
    // eslint-disable-next-line no-undef
    axios
      .get(`https://jsonplaceholder.typicode.com/users/${item.userId}`)
      .then((responseUser) => {
        const newUser = {
          id: item.id,
          title: item.title,
          completed: item.completed,
          user: {
            name: responseUser.data.name,
            username: responseUser.data.username,
            email: responseUser.data.email,
          },
        };
        console.log(newUser);
      });
  });

  return newTData;
};

const eventListeners = () => {
  document.querySelector("#fetchData_btn1").addEventListener("click", () => {
    console.log(fetchDataUsers());
  });
};

eventListeners();
