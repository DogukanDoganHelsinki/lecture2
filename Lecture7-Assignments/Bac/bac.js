const calculate = () => {
  const drinkSize = document.querySelector("#drink").value;
  const doses = document.querySelector("#doses").value;
  const alcohol = document.querySelector("#alcohol").value;
  const weight = document.querySelector("#weight").value;
  const time = document.querySelector("#time").value;
  const radioButtons = document.querySelectorAll("input[name=\"gender\"]");
  const result = document.querySelector("#result");

  const litres = drinkSize * doses;
  const grams = litres * 8 * alcohol;
  const burning = weight / 10;
  const gramsLeft = grams - burning * time;

  let selectedGender;

  for (const radioButton of radioButtons) {
    if (
      radioButton.checked &&
      !isNaN(doses) &&
      !isNaN(time) &&
      !isNaN(weight) &&
      !isNaN(alcohol)
    ) {
      selectedGender = radioButton.value;
      if (selectedGender === "male") {
        result.innerText = `Your BAC is ${gramsLeft / (weight * 0.7)}`;
      } else {
        result.innerText = `Your BAC is ${gramsLeft / (weight * 0.6)}`;
      }
      break;
    } else {
      result.innerText =
        "Error! Please make sure you have selected all the options and your inputs are valid numbers";
    }
  }
};

document.querySelector("#form_bac").addEventListener("submit", (e) => {
  e.preventDefault();
  calculate();

  document.querySelector("#reset_btn").addEventListener("click", () => {
    result.innerText = "Not calculated";
  });
});
