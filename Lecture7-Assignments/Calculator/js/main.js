const numbers = document.querySelectorAll(".number");
const current = document.querySelector(".currentvalue");
const equals = document.querySelector(".equals");
const clear = document.querySelector(".clear");
const plus = document.querySelector("#plus");
const minus = document.querySelector("#minus");
const multiply = document.querySelector("#multiply");
const divide = document.querySelector("#divide");
const dot = document.querySelector("#dot");

numbers.forEach((button) => {
  button.addEventListener("click", (event) => {
    const newInput = event.target.textContent;

    current.value += newInput;

    const myArray = current.value.split("");
    const myArrayLastItem = myArray.slice(-1);

    if (
      myArray.includes("+") ||
      myArray.includes("-") ||
      myArray.includes("*") ||
      myArray.includes("/")
    ) {
      plus.classList.add("disabled");
      minus.classList.add("disabled");
      multiply.classList.add("disabled");
      divide.classList.add("disabled");
    }

    if (myArrayLastItem[0] === ".") {
      dot.classList.add("disabled");
    } else if (
      myArray.includes("+") ||
      myArray.includes("-") ||
      myArray.includes("*") ||
      myArray.includes("/")
    ) {
      dot.classList.remove("disabled");
    }
  });
});

equals.addEventListener("click", () => {
  current.value = eval(current.value);
  plus.classList.remove("disabled");
  minus.classList.remove("disabled");
  multiply.classList.remove("disabled");
  divide.classList.remove("disabled");
  dot.classList.remove("disabled");
});

clear.addEventListener("click", () => {
  current.value = "";
  plus.classList.remove("disabled");
  minus.classList.remove("disabled");
  multiply.classList.remove("disabled");
  divide.classList.remove("disabled");
  dot.classList.remove("disabled");
});
