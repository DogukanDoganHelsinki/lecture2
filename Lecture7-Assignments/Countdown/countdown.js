// Assignment 7.1: Countdown to Start (callbacks)

console.log("3");

setTimeout(() => {
  console.log("..2");
  setTimeout(() => {
    console.log("...1");
    setTimeout(() => {
      console.log("GO!");
    }, 1000);
  }, 1000);
}, 1000);
