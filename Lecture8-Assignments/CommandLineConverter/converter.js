const amount = parseFloat(process.argv[2]);
const source = process.argv[3];
const unit = process.argv[4];

export const converter = (amount, source, unit) => {
  let result;

  const sourceObj = {
    lt: 2484.3,
    dl: 248.43,
    oz: 73.5,
    cups: 591.4,
    pints: 1419.6,
  };
  const myKeys = Object.keys(sourceObj);
  console.log(myKeys);

  // if (Number.isNaN(amount) || !myKeys.includes(source) ){
  //   return "Error";
  // }

  result = amount * (sourceObj[`${source}`] / sourceObj[`${unit}`]);

  return `${amount} ${source} is ${result} ${unit}`;
};
console.log(converter(amount, source, unit));
