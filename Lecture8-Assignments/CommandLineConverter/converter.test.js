import { converter } from "./converter.js";
import { expect, describe, it } from "@jest/globals";

describe("Testing numbers", () => {
  it("gives the correct result for positive numbers", () => {
    const result = converter(1, "lt", "dl");
    expect(result).toBe("1 lt is 10 dl");
  });
  it("gives the correct result for negative numbers", () => {
    const result = converter(-1, "lt", "dl");
    expect(result).toBe("-1 lt is -10 dl");
  });
  it("gives the correct result for decimal integers", () => {
    const result = converter(10.2, "lt", "dl");
    expect(result).toBe("10.2 lt is 102 dl");
  });
  it("gives the correct result for negative decimal integers", () => {
    const result = converter(-10.2, "lt", "dl");
    expect(result).toBe("-10.2 lt is -102 dl");
  });
});

describe("Testing unsuitable types", () => {
  it("should not give an error if the first argument is string", () => {
    const result = converter("test", "lt", "dl");
    expect(result).not.toBe("Error");
  });
  it("should give NaN result if the last argument is not a suitable parameter", () => {
    const result = converter("10", "lt", "dll");
    expect(result).toBe("10 lt is NaN dll");
  });
});
