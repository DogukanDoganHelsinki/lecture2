import express from "express";

const server = express();
server.use(express.json());

let notes = [];

server.get("/", (req, res) => {
  res.send(notes);
});

server.post("/", (req, res) => {
  const { id, headline, text } = req.body;

  if (!id || !headline || !text) {
    res.status(404).send("Missing Info");
  }

  notes = notes.concat({ id, headline, text });
  res.status(201).send("Note created");
});

server.delete("/:id", (req, res) => {
  const id = Number(req.params.id);

  const newNotes = notes.filter((note) => note.id !== id);
  if (newNotes.length === notes.length) {
    return res.status(404).send();
  }
  notes = newNotes;
  res.status(204).send();
});

const PORT = 3000;
server.listen(PORT, () => console.log("Listening to port", PORT));
