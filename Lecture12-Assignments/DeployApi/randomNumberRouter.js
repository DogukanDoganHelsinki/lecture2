import express from "express";

const router = express.Router();

router.get("/", (req, res) => {
  const first = Number(req.query.first);
  const second = Number(req.query.second);
  const isInteger = req.query.int;

  if (!first || !second) {
    res.status(404).send("Invalid values");
  }

  if (isInteger === "yes") {
    const randomInt = (first, second) =>
      Math.floor(Math.random() * (first - second + 1)) + second;
    return res.send(
      `Your random number between ${first} and ${second} is: ` +
        randomInt(first, second)
    );
  }

  const random = (first, second) =>
    Math.random() * (first - second + 1) + second;
  res.send(
    `Your random number between ${first} and ${second} is: ` +
      random(first, second)
  );
});

export default router;
