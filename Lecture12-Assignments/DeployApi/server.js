import express from "express";
import randomNumberRouter from "./randomNumberRouter.js";

const server = express();
server.use(express.json());

server.use("/random", randomNumberRouter);
server.use(express.static("public"));

export default server;
