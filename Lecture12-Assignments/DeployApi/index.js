import "dotenv/config";
import server from "./server.js";

const PORT = process.env.PORT || 3001;

server.listen(PORT, () => console.log(`Listening port ${PORT}`));
