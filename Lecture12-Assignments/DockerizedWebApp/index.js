import express from "express";

const server = express();

server.get("/", (req, res) => {
  res.send("Success");
});

server.listen(3000);
