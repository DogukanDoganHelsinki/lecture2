const submit = () => {
  const name = document.querySelector("#name").value;
  const textArea = document.querySelector("#textarea").value;
  const createdPost = document.querySelector(".createdPost");

  if (name.length === 0 || textArea.length === 0) {
    return alert("Please fill out the form");
  }

  let newPostHeadDiv = document.createElement("div");
  let newPostTextDiv = document.createElement("div");
  let newPostHeadAndTextDiv = document.createElement("div");
  let newPostHeadline = document.createElement("h4");
  let newPostParagraph = document.createElement("p");
  let newPostDeleteBtn = document.createElement("button");

  newPostHeadline.textContent = name;
  newPostHeadline.classList.add("newPostHeadline");

  newPostDeleteBtn.textContent = "Delete";
  newPostDeleteBtn.classList.add("delete_btn");

  newPostParagraph.textContent = textArea;
  newPostParagraph.classList.add("newPostParagraph");

  newPostHeadDiv.appendChild(newPostHeadline);
  newPostHeadDiv.appendChild(newPostDeleteBtn);
  newPostHeadDiv.classList.add("newPostHeadDiv");

  newPostTextDiv.appendChild(newPostParagraph);

  newPostHeadAndTextDiv.appendChild(newPostHeadDiv);
  newPostHeadAndTextDiv.appendChild(newPostTextDiv);
  newPostHeadAndTextDiv.classList.add("newPostHeadAndTextDiv");

  createdPost.appendChild(newPostHeadAndTextDiv);
  createdPost.classList.add("createdPostDiv");
};

const hideForum = () => {
  let focumContainer = document.querySelector("#forum-container");

  focumContainer.classList.toggle("forum-container");
};

const deletePost = (e) => {
  const deleteDiv = e.target.parentElement.parentElement;
  deleteDiv.remove();
};

const eventListeners = () => {
  document.querySelector("#submit_btn").addEventListener("click", () => {
    submit();
  });

  document.querySelector("#addPost_btn").addEventListener("click", () => {
    hideForum();
  });
  document.querySelector(".createdPost").addEventListener("click", (e) => {
    deletePost(e);
  });
};

eventListeners();
