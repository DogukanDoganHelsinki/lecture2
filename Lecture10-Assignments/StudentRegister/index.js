const summation = (n) => {
  const max = Number(n);
  console.log(max);

  let sum = 0;
  for (let i = 0; i <= n; i++) {
    sum = sum + i;
  }
  return sum;
};

const n = Number(process.argv[2]);
console.log(summation(n));
