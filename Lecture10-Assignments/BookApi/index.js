import express from "express";
import { errorHandler, authenticate } from "./middleware.js";
import bookRouter from "./bookRouter.js";
import bookUserRouter from "./bookUserRouter.js";
import "dotenv/config";

const server = express();
server.use(express.json());
server.use(bookUserRouter);
server.use("/api/v1/books", bookRouter);

server.use(errorHandler);

server.listen(process.env.PORT, () =>
  console.log(`Listening ${process.env.PORT}`)
);
