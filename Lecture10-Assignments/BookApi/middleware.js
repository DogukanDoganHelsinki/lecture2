import jwt from "jsonwebtoken";
import "dotenv/config";

export const validator = (req, res, next) => {
  const { id, name, author, read } = req.body;

  if (
    !id ||
    !name ||
    !author ||
    !read
    // typeof id !== "number" ||
    // typeof read !== "boolean"
  ) {
    return res.status(404).send("You should give an id, name, author and read");
  }

  next();
};

export const loggingMiddleware = (req, res, next) => {
  console.log(`Ip adress is : ${req.ip}`);
  console.log(`App is : ${req.app}`);
  console.log(`BaseUrl is : ${req.originalUrl}`);
  console.log(`Protocol is : ${req.protocol}`);
  console.log(`SignedCookies is : ${req.signedCookies}`);
  console.log(`Subdomains is : ${req.subdomains}`);
  next();
};

export const authenticate = (req, res, next) => {
  const auth = req.get("Authorization");
  if (!auth?.startsWith("Bearer ")) {
    return res.status(401).send("Invalid or missing token");
  }

  const token = auth.substring(7);
  try {
    const decodedToken = jwt.verify(token, process.env.SECRET);
    req.user = decodedToken;
    next();
  } catch (error) {
    return res.status(401).send("Invalid or missing token");
  }
};

export const errorHandler = (req, res) => {
  res.status(404).send("404 - Endpoint not found");
};

export const admin = (req, res, next) => {
  req.user.isAdmin ? next() : res.status(401).send("Missing admin privileges");
};
