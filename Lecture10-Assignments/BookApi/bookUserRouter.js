import express from "express";
import argon2 from "argon2";
import jwt from "jsonwebtoken";

const router = express.Router();

let users = [];

router.post("/api/v1/users/register", async (req, res) => {
  const { username, password } = req.body;

  const existingUser = users.find((user) => user.username === username);

  if (existingUser === undefined && username !== process.env.ADMIN_USERNAME) {
    const hashedPassword = await argon2.hash(password);

    const newUser = { username, hashedPassword };
    users = users.concat(newUser);

    const token = jwt.sign({ username }, hashedPassword);

    res.status(200).send(token);
  } else
    return res.send(
      "You already have an account, did you forget your password?"
    );
});

router.post("/api/v1/users/login", async (req, res) => {
  const { username, password } = req.body;

  const existingUser = users.find((user) => user.username === username);

  if (existingUser === undefined) {
    return res.status(401).send("Invalid username or password");
  }
  const isValidPassword = await argon2.verify(
    existingUser.hashedPassword,
    password
  );

  if (!isValidPassword) {
    return res.status(401).send("Invalid username or password");
  }

  const token = jwt.sign({ username }, password);
  res.status(200).send(token);
});

router.post("/admin", async (req, res) => {
  const { username, password } = req.body;

  const isAdminUsername = username === process.env.ADMIN_USERNAME;
  const isAdminPassword = await argon2.verify(
    process.env.ADMIN_PASSWORD,
    password
  );

  if (!isAdminUsername || !isAdminPassword) {
    return res.status(401).send("Invalid username or password");
  }

  const token = jwt.sign(
    { username: process.env.ADMIN_USERNAME, isAdmin: true },
    process.env.SECRET
  );

  res.status(200).send(token);
});

export default router;
