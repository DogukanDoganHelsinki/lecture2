import express from "express";
import { validator, loggingMiddleware, admin } from "./middleware.js";

const router = express.Router();

let books = [];

router.get("/", (req, res) => {
  res.status(200).send(books);
});

router.get("/:id", (req, res) => {
  res.status(201).send(books.find((book) => book.id === Number(req.params.id)));
});

router.post("/", validator, loggingMiddleware, admin, (req, res) => {
  const { id, name, author, read } = req.body;

  books = books.concat({ id, name, author, read });
  res.status(201).send("Created");
});

router.put("/:id", validator, loggingMiddleware, admin, (req, res) => {
  const { name, author, read } = req.body;

  const id = Number(req.params.id);
  const book = books.find((book) => book.id === id);
  if (!book) {
    return res.status(404).send();
  }

  const updatedBook = {
    id: book.id,
    name: name ?? book.name,
    author: author ?? book.author,
    read: read ?? book.read,
  };

  books = books.map((book) => (book.id === id ? updatedBook : book));
  res.status(204).send("Changed");
});

router.delete("/:id", admin, (req, res) => {
  const id = Number(req.params.id);
  const newBookList = books.filter((book) => book.id !== id);

  if (newBookList.length === books.length) {
    return res.status(404).send("Error!");
  }
  books = newBookList;
  res.status(204).send("Deleted");
});

export default router;
