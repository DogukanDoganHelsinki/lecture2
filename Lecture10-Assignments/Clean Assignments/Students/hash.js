import argon2 from "argon2";

const password = process.argv[2];
argon2.hash(password).then((result) => console.log(result));

//$argon2id$v=19$m=65536,t=3,p=4$n1ZBDkCmIIpDIcMMrRrpNA$tM+q8URGdYFmpQYQ7lhPrORbL6s5LnaF+81slPfXgVc
//$argon2id$v=19$m=65536,t=3,p=4$QyvNcZaM05hGrfOW9RgI/g$0j0L8A8Z9SUgrLqppdiINBlfJnBWJHeaVaopgvg/Uvg
//$argon2id$v=19$m=65536,t=3,p=4$Z2fLT42AN6RLPZy7tu8B3A$CdEXE+kodQ+kzwxfgJoYn8eZY3DSNWlDgLmuscvrs70
