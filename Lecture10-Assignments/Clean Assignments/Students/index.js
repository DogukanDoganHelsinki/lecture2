import express from "express";
import { logger, notFound } from "./middleware.js";
import studentRouter from "./studentsRouter.js";
import userRouter from "./userRouter.js";
import "dotenv/config";

const server = express();
server.use(express.json());
server.use(logger);
server.use("/students", studentRouter);
server.use("/user", userRouter);

server.use(express.static("public"));
server.use(notFound);

server.listen(process.env.PORT, () =>
  console.log("Listening to port", process.env.PORT)
);
