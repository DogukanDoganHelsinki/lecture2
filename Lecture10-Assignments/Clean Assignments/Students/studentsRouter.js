import express from "express";

const router = express.Router();

let students = [];

router.get("/", (req, res) => {
  res.send(students);
});

router.get("/:id", (req, res) => {
  const id = Number(req.params.id);

  const selectedStudent = students.find((student) => student.id === id);

  if (selectedStudent === null) {
    return res.status(404).send();
  }

  res.status(200).send(selectedStudent);
});

router.post("/", (req, res) => {
  const { id, name, email } = req.body;

  if (!id || !name || !email) {
    return res.status(400).send("Invalid ID, NAME or EMAIL");
  }

  const newStudent = { id, name, email };

  students = students.concat(newStudent);

  res.status(201).send("Created");
});

router.put("/:id", (req, res) => {
  const { name, email } = req.body;

  if (!name && !email) {
    return res.status(400).send("Change the values");
  }

  const id = Number(req.params.id);

  let selectedStudent = students.find((student) => student.id === id);
  if (!selectedStudent) {
    return res.status(404).send();
  }

  const updatedStudent = {
    id: selectedStudent.id,
    name: name ?? selectedStudent.name,
    email: email ?? selectedStudent.email,
  };

  students = students.map((student) =>
    student.id === id ? updatedStudent : student
  );

  res.status(204).send();
});

router.delete("/:id", (req, res) => {
  const id = Number(req.params.id);

  const newStudents = students.filter((student) => student.id !== id);

  if (newStudents.length === students) {
    return res.status(404).send();
  }

  students = newStudents;
  res.status(204).send();
});

export default router;
