// Assignment 6.6: ToDo List

const addTodo = (e) => {
  let input = document.querySelector("#input");
  const list = document.querySelector("#ul");

  if (e.keyCode == 13) {
    let newTodo = document.createElement("li");
    newTodo.textContent = input.value;
    list.appendChild(newTodo);
    input.value = "";
  }
};

const markDone = (e) => {
  e.target.classList.toggle("done");
};

const removeAll = () => {
  let list = document.querySelector("#ul");

  if (list !== 0) {
    list.remove();
    location.reload();
  }
};

const eventListeners = () => {
  document.querySelector("#input").addEventListener("keydown", (e) => {
    addTodo(e);
  });
  document.querySelector("#ul").addEventListener("click", (e) => {
    markDone(e);
  });
  document.querySelector("#remove_btn").addEventListener("click", () => {
    removeAll();
  });
};

eventListeners();
