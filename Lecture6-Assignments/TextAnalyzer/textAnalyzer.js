// Assignment 6.5: Text analyzer (Ignore punctuation and capitalization is missing)

const analyze = () => {
  const text = document.querySelector("#text");

  const totalCountOfWords = text.value.split(" ");

  let average = 0;
  for (let i = 0; i < totalCountOfWords.length; i++) {
    average += totalCountOfWords[i].length / totalCountOfWords.length;
  }

  const groupedWords = totalCountOfWords.reduce((allWords, word) => {
    if (word in allWords) {
      allWords[word]++;
    } else {
      allWords[word] = 1;
    }
    return allWords;
  }, {});

  let sortedArray = [];
  for (let word in groupedWords) {
    sortedArray.push([word, groupedWords[word]]);
  }

  sortedArray.sort(function (a, b) {
    return b[1] - a[1];
  });

  for (let i = 0; i < sortedArray.length; i++) {
    const orderedWordsList = document.createElement("p");
    orderedWordsList.innerText = `${sortedArray[i]}`;
    document
      .getElementById("analyze_result_words")
      .appendChild(orderedWordsList);
  }

  document.querySelector(
    "#anaylze_result"
  ).innerText = `Total count of the word is ${totalCountOfWords.length}\n\n Average length of the words is ${average}`;
};

document.querySelector("#analyze_btn").addEventListener("click", () => {
  analyze();
});
